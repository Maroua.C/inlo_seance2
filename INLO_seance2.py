#!/usr/bin/env python
# -*- coding: utf-8 -*-

#changer les commentaire en anglais
import argparse
import sys

FILE_PATH = "/amuhome/c19012324/Bureau/INLO/seance 2/pymetacline/scripts/OTX.fasta"

INPUT_SEQ = []

def create_parser():
    program_description = '''
    What the program does.
'''

    parser = argparse.ArgumentParser(add_help=True, description=program_description)
      
#command for print the type of file
    parser.add_argument('-i', '--inputfile', default=sys.stdin, metavar="FASTA", type=argparse.FileType('r'), required=True)



#commande for read and print all the file
    parser.add_argument('-r', '--readfile', default=sys.stdin, metavar="FILE", type=str, required=False)


#command for print a specific sequence depend to the identifiant
#nargs= + for more than one argument available
    parser.add_argument('-Aseq', '--accensionsequence', default=sys.stdin, metavar="FILE", type=str, nargs='+', required=False)

#command for print a specific sequence depend to the identifiant
    parser.add_argument('-Nseq', '--numbersequence', default=sys.stdin, metavar="FILE", type=str, nargs='+', required=False)


#command for print sequence in  a  fine
    parser.add_argument('-f', '--fileprint', default=sys.stdin, metavar="FILE", type=str, required=False)

    return parser



def run_file(the_file):
    file = open(str(the_file), 'r')     
#print all the file
    read_file = file.read() 
    print(read_file)
    file.close()


def search_by_id(the_file):
    file = open(str(the_file), 'r')
    read_file = file.readlines()
    #print(read_file)    
    i = 0 
    i_choice = 0   
    #ENST00000428120    
    choice0 = input("Tape the identifiant/Accension : ", )
    choice = '>' + choice0
    print(len(read_file))
    
    for line in read_file:       
        if line.strip() == choice:
            print("Identifiant/Accension")
            print(line)
            #save the line where we have choice          
            i_choice = i
            #print(i)
            print("Sequence")
            print(read_file[i_choice+1])            
            INPUT_SEQ.append(read_file[i_choice])
            INPUT_SEQ.append(read_file[i_choice+1])                    
        i = i + 1
    if i_choice == 0:
        print(" Accension/Identifiant wasn't found")
    file.close()
   
def search_by_numbersequence(the_file):
    file = open(str(the_file), 'r')
    read_file = file.readlines()
	
    choice0 = int(input("Tape the number : ", ))
	
	# fill the dictionnary key = id and value= sequence 
    for i in range(0, len(read_file), 2):
		
        if choice0 == i:
            print("number of sequence :",i)
            print("\n sequence :",read_file[i+1])
			
        
    file.close()


#faire commande pour rentrer lasequence fasta dans un fichier
def put_in_file():
	 file = open("seq.txt", "w")
	 #print(INPUT_SEQ)
	 i = 0
	 for i in range(0, len(INPUT_SEQ)):
	 	 file.write(INPUT_SEQ[i])	 
	 file.close()

def main():
	
    parser = create_parser()
    args = parser.parse_args()
    args = dict(args.__dict__) 
    
    #print the name and the type of the file
    print(args['inputfile'])  
    
    #print all the file
    the_file = args['readfile']
    run_file(the_file)
	
	#print the sequence of the identifiant that the user enter
    the_file = args['accensionsequence']
    search_by_id(the_file)
    
	#print the sequence of the number that the user enter
    the_file = args['numbersequence']
    search_by_numbersequence(the_file)
    
	
	#put the sequence and the id in a file 
    the_file = args['fileprint']
    put_in_file()

   
if __name__ == "__main__":
	main()






